#include <msp430.h> 
#include <stdint.h>

/* Functieprototypen voor de meegeleverde "oled_lib" */

/* Deze functie configureert het display
 * via i2c en tekent alvast het kader.
 */




void initDisplay();

/* Deze functie past de weergeven
 * temperatuur aan.
 *
 * int temp: De te weergeven temperatuur.
 * Max is 999 min is -99. Hierbuiten weer-
 * geeft het "Err-"
 *
 * Voorbeeld:
 * setTemp(100); //stel 10.0 graden in
 */
void setTemp(int temp);

/* Deze functie past de bovenste titel aan.
 * char tekst[]: de te weergeven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); //laat Hallo zien
 */
void setTitle(char tekst[]);

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	//stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */



	initDisplay();
	setTitle("Opdracht7.1.13");

	uint8_t temperatuur=0;

	while(1)
	{

	}
}
